# four-to-one-balun

4:1 balun for "T" match antennas

## License

&copy; 2015 [Libre Space Foundation](http://librespacefoundation.org).

Licensed under the [CERN OHLv1.2](LICENSE).